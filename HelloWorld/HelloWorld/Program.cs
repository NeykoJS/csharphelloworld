﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Saisir nom : ");
            string nom = Console.ReadLine();
            Console.WriteLine("Saisir Prenom : ");
            string prenom = Console.ReadLine();
            Console.WriteLine("Saisir Age : ");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine(string.Format("Salutation {0} {1}", nom, prenom));
            Console.WriteLine("Vous avez {0} ans", age);
            int annee = DateTime.Now.Year;
            int date = annee - age;
            Console.WriteLine("Vous etes nee en {0}", date);
            if ((date%4==0) && (date%100!=0) || (date%400==0))
            {
                Console.WriteLine("ah oui oui oui annee bissextile");
            }
            else
            {
                Console.WriteLine("Ah la non je dis non, pas annee bissextile");
            }
            for (int counter = 1; counter<=12; counter++)
            {
                Console.WriteLine("Counter = {0}", counter);
            }
            Console.ReadKey();
        }
    }
}
